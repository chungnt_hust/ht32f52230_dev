/*
 * systemClock.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn

    Configure the system clock in startup file (startup_ht32fxxxx.s)
    by calling SystemInit function. Please refer to system_ht32fxxxx.c.

    If no system clock is configured, HSI 8MHz is the system clock by default.

    Configure system clock frequency and HCLK prescaler.
    External (4-16MHz) 8 MHz crystal is used to drive the system clock, max = 48MHz.

    Use FLASH_SetWaitState (FMCEN) if enabled during Sleep mode
    Set and reset by software. Users can set FMCEN as 0 to reduce power consumption
    if the Flash Memory is unused during Sleep mode
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "systemClockCfg.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
/* PLL_CLK_OUT = CLK_IN*NF2/NO2 */
/*
 * NOTE !!!! VCO_out = 48~96 = PLL_OUT * NO2 * 2 
 */
#define CLK_IN 8    // 8MHz clock in

#define NO2_1  1 
#define NO2_2  2   
#define NO2_4  4 
#define NO2_8  8 

#define NF2_16 0 // = 16 
#define NF2_1  1 
#define NF2_2  2 
#define NF2_3  3 
#define NF2_4  4 
#define NF2_5  5 
#define NF2_6  6 
#define NF2_7  7 
#define NF2_8  8 
#define NF2_9  9 
#define NF2_10 10 
#define NF2_11 11 
#define NF2_12 12 
#define NF2_13 13 
#define NF2_14 14 
#define NF2_15 15 

#define PLL_CFG (((u32)NO2_1 << 23) | ((u32)NF2_12 << 21)) // PLL_CLK_OUT = 8 * 12 / 1

#define HCLK_CFG    CKCU_SYSCLK_DIV2    // HCLK   24MHz
#define CORE_CLK_MHZ 24
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
sysTickCB funcSysTickCB;
volatile u32 timeDelay = 0;

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static CKCU_ClocksTypeDef ClockFreq;
static CKCU_PLLInitTypeDef PLLInit;

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void SysClockCfg_HSI(void)
{
  ErrStatus ClockStatus;

  /* Reset CKCU, SYSCLK = HSI */
  CKCU_DeInit();

  /* Enable HSI */
  CKCU_HSICmd(ENABLE);

  /* Wait until HSI is ready*/
  while (CKCU_GetClockReadyStatus(CKCU_FLAG_HSIRDY) != SET);

  /* PLL configuration */
  PLLInit.ClockSource = CKCU_PLLSRC_HSI;
  PLLInit.CFG = PLL_CFG;
  PLLInit.BYPASSCmd = DISABLE;
  CKCU_PLLInit(&PLLInit);

  CKCU_PLLCmd(ENABLE);

  /* Wait until PLL is ready */
  while(CKCU_GetClockReadyStatus(CKCU_FLAG_PLLRDY) == RESET);

  // /* FLASH wait state configuration */
  // FLASH_SetWaitState(FLASH_WAITSTATE_0);  /* FLASH zero wait clock */

  /* HCLK = SYSCLK/x */
  CKCU_SetHCLKPrescaler(HCLK_CFG);

  /* Configure PLL as system clock */
  ClockStatus = CKCU_SysClockConfig(CKCU_SW_PLL);

  if(ClockStatus != SUCCESS)
  {
    while(1);
  }                 

  /* Get the current clocks setting */
  CKCU_GetClocksFrequency(&ClockFreq);
}

void SysClockCfg_HSE(void)
{
  ErrStatus ClockStatus;

  /* Reset CKCU, SYSCLK = HSI */
  CKCU_DeInit();

  /* Enable HSE */
  CKCU_HSECmd(ENABLE);

  /* Wait until HSE is ready or time-out */
  ClockStatus = CKCU_WaitHSEReady();

  if(ClockStatus == SUCCESS)
  {
    /* PLL configuration */
    PLLInit.ClockSource = CKCU_PLLSRC_HSE;
    PLLInit.CFG = PLL_CFG;
    PLLInit.BYPASSCmd = DISABLE;
    CKCU_PLLInit(&PLLInit);

    CKCU_PLLCmd(ENABLE);

    /* Wait until PLL is ready */
    while(CKCU_GetClockReadyStatus(CKCU_FLAG_PLLRDY) == RESET);

    // /* FLASH wait state configuration */
    // FLASH_SetWaitState(FLASH_WAITSTATE_0);  /* FLASH zero wait clock */

    /* HCLK = SYSCLK/x */
    CKCU_SetHCLKPrescaler(HCLK_CFG);

    /* Configure PLL as system clock */
    ClockStatus = CKCU_SysClockConfig(CKCU_SW_PLL);

    if(ClockStatus != SUCCESS)
    {
      while(1);
    }
  }
  else
  {
    /* HSE is failed. User can handle this situation here. */
    while(1);
  }                    

  // /* Get the current clocks setting */
  // CKCU_GetClocksFrequency(&ClockFreq);

  // /* Enable HSE clock monitor & interrupt, once HSE is failed the NMI exception will occur */
  // CKCU_CKMCmd(ENABLE);
  // CKCU_IntConfig(CKCU_INT_CKSIE, ENABLE);
}

void SysClockCfg_CKOUTConfig(void)
{
  CKCU_CKOUTInitTypeDef CKOUTInit;

  CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
  CKCUClock.Bit.AFIO       = 1;
  CKCU_PeripClockConfig(CKCUClock, ENABLE);

  AFIO_GPxConfig(GPIO_PA, AFIO_PIN_9, AFIO_FUN_SYSTEM);
  CKOUTInit.CKOUTSRC = CKCU_CKOUTSRC_HCLK_DIV16;
  CKCU_CKOUTConfig(&CKOUTInit);
}

void SysClockCfg_RegCallback(sysTickCB func)
{
  /* SYSTICK configuration */
  SYSTICK_ClockSourceConfig(SYSTICK_SRC_STCLK);      
  SYSTICK_SetReloadValue(SystemCoreClock / CORE_CLK_MHZ / 1000); // (CK_AHB/CORE_CLK_MHZ/1000) = 1ms on chip
  SYSTICK_IntConfig(ENABLE);                          // Enable SYSTICK Interrupt
  SYSTICK_CounterCmd(SYSTICK_COUNTER_CLEAR);
  SYSTICK_CounterCmd(SYSTICK_COUNTER_ENABLE);
  funcSysTickCB = func;
}

void SysClockDelay(u32 time)
{
  timeDelay = time;
  while(timeDelay > 0);
}
